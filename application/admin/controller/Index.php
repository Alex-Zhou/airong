<?php

namespace app\admin\controller;

use think\Db;

class Index extends AdminController
{
    // 首页页面
    public function index()
    {
        return view('index/index');
    }

    // 分页页面
    public function welcome()
    {
        // 服务器IP
        $ip = $_SERVER['SERVER_ADDR'];
        // 服务器版本
        $server = php_uname('s').php_uname('r');
        // 端口号
        $port = $_SERVER['SERVER_PORT'];
        // 服务器操作系统
        $server_action = php_uname();
        // PHP版本
        $php = PHP_VERSION;
        // PHP运行方式
        $php_action = php_sapi_name();
        // Mysql版本
        $mysql = Db::query('SELECT version()')[0]['version()'];
        // THINKPHP 版本
        $think = THINK_VERSION;
        // 上传附件限制
        $max_upload = get_cfg_var ('upload_max_filesize')?get_cfg_var ('upload_max_filesize'):'不允许';
        // 最大执行时间
        $max_time = get_cfg_var('max_execution_time').'s';
        // 服务器系统目录
        $server_dir = $_SERVER['SERVER_PROTOCOL'];

        return view('index/welcome', [
            'ip'            => $ip,
            'server'        => $server,
            'server_action' => $server_action,
            'php'           => $php,
            'php_action'    => $php_action,
            'mysql'         => $mysql,
            'think'         => $think,
            'max_upload'    => $max_upload,
            'max_time'      => $max_time,
            'server_dir'    => $server_dir,
            'port'          => $port
        ]);
    }
}
