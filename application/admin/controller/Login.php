<?php
namespace app\admin\controller;

use think\Controller;
use app\admin\model\Login as loginModel;
use think\Session;
use think\Db;
use think\captcha\Captcha;

class Login extends Controller
{

    public function index()
    {
        // 判断是否已登录
        if ( Session::get('admin_user')) {
            // 如果已登录则重定向到后台首页
            $this->redirect('Index/index');exit;
        }
        return view('login/index');
    }
    public function doLogin()
    {
        // 判断验证码
        $code = $_POST['code'];
        $captcha = new Captcha();
        if( !$captcha->check($code))
        {
            $this->error('验证码错误','login/index');die;
        }

        // 验证数据库
        $userName = input('post.userName');
        $pwd = input('post.password');
        // 验证表单数据
        $res = Db::name('admin')
            ->field('password', true)
            ->where('adminName = "'.$userName.'" and password = "'.md5($pwd).'"')
            ->find();

        // 判断是否登录成功
        if (!$res) {
            $this->error('账号或密码错误, 请检查后重试!','Login/index');exit;
        }

        // 判断用户状态
        if ($res['status'] != 1) {
            $this->error('账户已禁用, 请尝试联系超级管理员解封!', 'Login/index');exit;
        }

        if ($res['icon'] == null) {
            $res['icon'] = '/static/admin/face/batman.jpg';
        }

        // 储存session数据
        session('admin_user', $res);

        //根据用户id获取对应的节点信息
        $res = Db::name('node')->field('mname,aname')
            ->where('id in'.Db::name('role_node')->field('nid')
            ->where("rid in ".Db::name('admin_role')->field('rid')
            ->where(array('uid'=>array('eq',$res['id'])))->buildSql())->buildSql())->select();

        // 将节点信息重组
        $nodelist = array();
        $nodelist['Index'] = array('index', 'welcome');
        foreach ($res as $k) {
            $nodelist[$k['mname']][] = $k['aname'];
        }

        // 将节点信息存入SESSION中
        session('admin_user.nodelist', $nodelist);

        $this->success('登录成功','index/index');die;

    }

    // 退出登陆
    public function loginOut()
    {
        session(null);

        $this->success('安全退出!', 'Login/index');
    }
}