<?php

namespace app\admin\controller;

use think\Request;
use think\Db;
use think\Validate;

class Node extends AdminController
{
    // 展示 列表页
    public function index($key = null)
    {
        if (empty($key)) {
            $list = Db::name('node')
                ->field(['id', 'mname', 'name', 'aname', 'status'])
                ->paginate(4);
        } else {
            $map['name']  = ['like','%'.$key.'%'];
            $list = Db::name('node')
                ->field(['id', 'mname', 'name', 'aname', 'status'])
                ->where($map)
                ->paginate(4,false,['query'=>request()->param()]);
        }

        // 统计所有权限的 数量
        $count = count($list);

        // 渲染模板
        return view('node/index', [
            'list'  => $list,
            'count' => $count
        ]);
    }

    // 展示 添加权限页
    public function add()
    {
        // 渲染模板
        return view('node/add');
    }

    // 执行添加 权限
    public function doAdd(Request $request)
    {
        $data = input('post.');

        // 插入数据库
        $result = Db::name('node')->insert($data);

        // 判断是否成功添加
        if ($result > 0) {
            return $this->success('添加成功!', '/admin/Node/index');
        }else{
            return $this->error('添加失败!');
        }
    }

    // 编辑 权限
    public function edit($id)
    {
        // 查询管理员信息
        $res = Db::name('node')
            ->where(['id' => $id])
            ->find();

        // 返回数据
        return view('node/edit', [
            'res' => $res
        ]);
    }

    // 执行更新 权限数据
    public function doEdit($id, Request $request)
    {
        $data = input('post.');

        // 判断 是否将权限 禁用, 如果禁用 则 角色自动失去此 权限
        if ($data['status'] == 0) {
            Db::name('role_node')->where('nid', $id)->delete();
        }

        $res = Db::name('node')->where(['id' => $id])->update($data);

        if ($res) {
            return $this->success('更新成功!', 'node/index');
        } else {
            return $this->error('更新失败!');
        }
    }

    // 执行 删除权限 数据
    public function doDelete($id)
    {
        // 是否为 Ajax 请求
        if (!Request::instance()->isAjax()) {
            $this->error('您查找的页面不存在', '/admin');
        }

        // 删除 跟此 权限有关的所有 role_node
        $role_node = Db::name('role_node')->where('nid', $id)->delete();

        // 删除 权限
        $res = Db::name('node')->where('id', $id)->delete();

        return $res;
    }

}