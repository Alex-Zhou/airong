<?php

namespace app\admin\controller;

use think\Controller;
use think\Session;

class AdminController extends Controller
{
	public function _initialize()
	{
		//判断session是否存在
		if( empty(Session::get('admin_user')) ){
			//重定向到 登陆页
			$this->redirect('Login/index');
		}

		//权限过滤
		$cname = request()->controller(); //获取控制器名
		$aname = request()->action(); //获取方法名

		$node = Session::get('admin_user.nodelist'); //获取权限列表

		if( Session::get('admin_user.adminName') != 'admin'){
			//验证操作权限
			if(empty( $node[$cname] ) || !in_array( $aname, $node[$cname] )){
				$this->error("抱歉！您没有操作权限！");
				exit;
			}
		}

	}


}