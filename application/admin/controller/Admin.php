<?php

namespace app\admin\controller;

use think\Request;
use think\Db;
use think\Validate;

class Admin extends AdminController
{
    public function index($key = null)
    {
        if (empty($key)) {
            $list = Db::name('admin')
                ->field('password', true)
                ->paginate(4);
        } else {
            $map['adminname']  = ['like','%'.$key.'%'];
            $list = Db::name('admin')
                ->field('password', true)
                ->where($map)
                ->paginate(4,false,['query'=>request()->param()]);
        }

        // 统计管理员数量
        $count = count($list);

        // 通过管理员id查询所拥有的权限
        foreach ($list as $v) {
            $role_ids = Db::name('admin_role')->field('rid')->where(['uid' => $v['id']])->select();
            //定义空数组
            $roles = array();
            //遍历
            foreach ($role_ids as $value) {
                $roles[] = Db::name('role')->where(['id' => $value['rid'], 'status' => 1])->field('name')->find();
            }
            $v['role'] = $roles; //将新得到角色信息放置到$v中
            $arr[] = $v;
        }

        return view('admin/index', [
            'list' => $arr,
            'count' => $count,
            'page' => $list
        ]);
    }

    public function add()
    {
        return view('admin/add');
    }

    public function doAdd(Request $request)
    {
        $data = input('post.');
        if ($data['password'] !== $data['repassword']) {
            $this->error('两次密码输入有误!');
        }

        unset($data['repassword']);
        $data['password'] = md5($data['password']);

        $result = Db::name('admin')->insert($data);

        if ($result > 0) {
            return $this->success('添加成功!', 'admin/Admin/index');
        } else {
            return $this->error('添加失败!');
        }
    }

    // 查询单个管理员信息
    public function show($id)
    {
        // 查询管理员信息
        $res['user'] = Db::name('admin')->field('password', true)->where(['id' => $id])->find();

        // 显式查询 此管理员的 角色id
        $row = Db::name('admin_role')->field('rid')->where(['uid' => $id])->select();

        // 遍历查询此管理员 拥有的 角色
        foreach ($row as $k) {
            $res['role'][] = Db::name('role')->where(['status' => 1, 'id' => $k['rid']])->find();
        }

        return view('admin/show', [
            'res' => $res
        ]);
    }

    public function rolelist($id)
    {
        //查询角色信息
        $list = Db::name('role')->where(['status' => 1])->select();
        //查询当前用户信息
        $users = Db::name('admin')->where(['id' => $id])->find();
        //获取当前用户的角色信息
        $rolelist = Db::name('admin_role')->where(['uid' => $id])->select();

        $myrole = array(); //定义空数组
        //对用户的角色进行重组
        foreach ($rolelist as $v) {
            $myrole[] = $v['rid'];
        }

        // 渲染模板
        return view('admin/rolelist', [
            'list' => $list,
            'users' => $users,
            'myrole' => $myrole
        ]);
    }

    // 编辑页面
    public function edit($id)
    {
        //查询角色信息
        $list = Db::name('role')->where(['status' => 1])->select();
        //获取当前用户的角色信息
        $rolelist = Db::name('admin_role')->where(['uid' => $id])->select();

        $myrole = array(); //定义空数组
        //对用户的角色进行重组
        foreach ($rolelist as $v) {
            $myrole[] = $v['rid'];
        }

        // 查询管理员信息
        $res = Db::name('admin')->field('password', true)->where(['id' => $id])->find();

        // 返回数据
        return view('admin/edit', [
            'list'   => $list,
            'myrole' => $myrole,
            'res'    => $res
        ]);
    }

    // 更新数据
    public function doEdit()
    {
        // 是否为 Ajax 请求
        if (!Request::instance()->isAjax()) {
            $this->error('您查找的页面不存在', '/admin');
        }

        // 获取数据
        $id = input('post.id');
        $data['adminName'] = input('post.adminName');
        $data['password'] = md5(input('post.password'));
        $data['repassword'] = md5(input('post.repassword'));

        if (!empty($data['password'])) {
            if ($data['password'] != $data['repassword']) {
                return 3;
            }
        }

        unset($data['repassword']);

        $res = Db::name('admin')->where('id', $id)->update($data);

        if ($res) {
            return 1;
        } else {
            return 0;
        }
    }

    // 停用用户
    public function stop($id)
    {
        // 是否为 Ajax 请求
        if (!Request::instance()->isAjax()) {
            $this->error('您查找的页面不存在', '/admin');
        }

        // 判断如果是管理员则直接返回
        if ($id == 1) {
            return 3;
        }

        $data['status'] = 0;

        $res = Db::name('admin')->where(['id' => $id])->update($data);

        return $res;
    }

    // 执行 分配角色 方法
    public function savenode(Request $request)
    {
        $p = input('post.');

        //判读必须选择一个角色
        if (empty($p['role'])) {
            $this->error("请选择一个角色！");
        }

        $uid = $p['uid'];
        // 判断是否存在当前用户
        $row = Db::name('admin')->where(['id' => $uid])->find();
        if ($row == 0) {
            return $this->error('页面出错', '/admin/Admin/index');
        }

        //清除用户所有的角色信息，避免重复添加
        Db::name('admin_role')->where(['uid' => $uid])->delete();

        foreach ($p['node'] as $v) {
            $data['uid'] = $uid;
            $data['rid'] = $v;
            //执行添加
            Db::name('admin_role')->insert($data);
        }

        return $this->success('角色分配成功', 'Admin/index');
    }

    // 启用用户
    public function enable()
    {
        // 是否为 Ajax 请求
        if (!Request::instance()->isAjax()) {
            $this->error('您查找的页面不存在', '/admin');
        }

        // 接收 参数
        $id = input('post.id');
        // 定义 数据
        $data['status'] = 1;

        // 执行
        $res = Db::name('admin')->where('id', $id)->update($data);

        // 返回 结果
        return $res;
    }
}