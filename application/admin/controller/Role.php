<?php

namespace app\admin\controller;

use think\Request;
use think\Db;
use think\Validate;

class Role extends AdminController
{
    public function index($key = null)
    {
        if (empty($key)) {
            $list = Db::name('role')
                ->field(['id', 'remark', 'name', 'status'])
                ->paginate(4,false,['query'=>request()->param()]);
        } else {
            $map['name']  = ['like','%'.$key.'%'];
            $list = Db::name('role')
                ->field(['id', 'remark', 'name', 'status'])
                ->where($map)
                ->paginate(4,false,['query'=>request()->param()]);
        }

        // 统计角色的数量
        $count = count($list);

        // 渲染模板
        return view('role/index', [
            'list'  => $list,
            'count' => $count
        ]);
    }

    public function add()
    {
        return view('role/add');
    }

    public function doAdd(Request $request)
    {
        $data = input('post.');

        $result = Db::name('role')->insert($data);

        if ($result > 0) {
            return $this->success('添加成功!', 'role/index');
        }else{
            return $this->error('添加失败!');
        }
    }

    // 查询单个 角色
    public function show($id)
    {
        // 申明变量
        $res = null;

        // 查询角色信息
        $res['role'] = Db::name('role')->where(['id' => $id])->find();

        // 查询 此角色的 权限id
        $row = Db::name('role_node')->field('nid')->where(['rid' => $id])->select();

        // 遍历查询此管理员 拥有的 角色
        foreach ($row as $k) {
            $res['node'][] = Db::name('node')->where(['status' => 1, 'id' => $k['nid']])->find();
        }

        return view('role/show', [
           'list' => $res
        ]);
    }

    // 编辑 角色
    public function edit($id)
    {
        // 查询管理员信息
        $res = Db::name('role')
            ->where(['id' => $id])
            ->find();

        // 返回数据
        return view('role/edit', [
            'res' => $res
        ]);
    }

    // 执行更新 角色数据
    public function doEdit($id, Request $request)
    {
        $data = input('post.');

        // 判断 是否将角色 禁用, 如果禁用 则 管理员自动失去此 角色
        if ($data['status'] == 0) {
            Db::name('admin_role')->where('rid', $id)->delete();
        }

        $res = Db::name('role')->where(['id' => $id])->update($data);

        if ($res > 0) {
            return $this->success('更新成功!', 'Role/index');
        }else{
            return $this->error('更新失败!');
        }
    }

    // 执行 删除角色 数据
    public function doDelete($id)
    {
        // 是否为 Ajax 请求
        if (!Request::instance()->isAjax()) {
            $this->error('您查找的页面不存在', '/admin');
        }

        $admin_role = Db::name('admin_role')->where('rid', $id)->delete();


        $role_node = Db::name('role_node')->where('rid', $id)->delete();

        $res = Db::name('role')->where('id', $id)->delete();

        return $res;
    }

    public function nodelist($id)
    {
        // 判断是否传正确的值
        if (!preg_match("/^\d*$/", $id)) {
            return $this->error('404 NOT FOUND!', '/admin/User/index');
        }

        //查询节点信息
        $list = Db::name('node')->where(['status' => 1])->select();
        //查询当前角色信息
        $roles = Db::name('role')->where(['id' => $id])->find();
        //获取当前角色的权限信息
        $nodelist = Db::name('role_node')->where(['rid' => $id])->select();

        $mynode = array(); // 定义空数组

        //对用户的角色进行重组
        foreach ($nodelist as $v) {
            $mynode[] = $v['nid'];
        }

        // 渲染模板
        return view('role/nodelist', [
            'list' => $list,
            'roles' => $roles,
            'mynode' => $mynode
        ]);
    }

    public function savenode(Request $request)
    {
        $p = input('post.');
        //判读必须选择一个角色
        if (empty($p['node'])) {
            $this->error("请选择一个角色！");
        }
        $rid = $p['rid'];
        // 判断是否存在当前角色
        $row = Db::name('role')->where(['id' => $rid])->find();
        if ($row == 0) {
            return $this->success('页面出错, 不要搞事情(╬◣д◢)', '/admin/Role/index');
        }

        //清除角色所有的权限信息，避免重复添加
        Db::name('role_node')->where(['rid' => $rid])->delete();

        foreach ($p['node'] as $v) {
            $data['rid'] = $rid;
            $data['nid'] = $v;
            //执行添加
            Db::name('role_node')->insert($data);
        }

        return $this->success('权限分配成功', '/admin/Role/index');
    }

}